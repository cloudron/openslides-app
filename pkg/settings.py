# Use 'DEBUG = True' to get more details for server errors.
# SECURITY WARNING: Don't run with debug turned on in production!
DEBUG = False

# Increasing Upload size to 100mb (default is 2.5mb)
DATA_UPLOAD_MAX_MEMORY_SIZE = 104857600

# Controls the verbosity on errors during a reset password. If enabled, an error
# will be shown, if there does not exist a user with a given email address. So one
# can check, if a email is registered. If this is not wanted, disable verbose
# messages. An success message will always be shown.
RESET_PASSWORD_VERBOSE_ERRORS = False

# Controls if electronic voting (means non-analog polls) are enabled.
ENABLE_ELECTRONIC_VOTING = True

# SAML integration
# Requires additional setup of SAML parameters in the /app/data/saml_settings.json to work!
# Instructions are in /app/code/server/openslides/saml/README.md
ENABLE_SAML = False

# Jitsi integration
JITSI_DOMAIN = None
JITSI_ROOM_NAME = None
JITSI_PASSWORD = None

# Internationalization
TIME_ZONE = 'America/New_York'
