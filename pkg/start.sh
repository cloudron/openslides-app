#!/bin/bash

set -eu

echo "=> Generating nginx.conf"
sed -e "s,##HOSTNAME##,${CLOUDRON_APP_DOMAIN}," \
    /app/pkg/nginx.conf  > /run/nginx.conf

echo "=> Configuring OpenSlides"
export DJANGO_SETTINGS_MODULE="cloudron_settings"

if [ ! -f "/app/data/settings.py" ]; then
  cp /app/pkg/settings.py /app/data/settings.py
fi

if [ ! -f "/app/data/saml_settings.json" ]; then
  cp /app/pkg/saml_settings.json /app/data/saml_settings.json
fi

if [ ! -f "/app/data/.secret_key" ]; then
  python3 /app/code/server/manage.py shell -c 'from django.core.management import utils; print(utils.get_random_secret_key())' > /app/data/.secret_key
fi

if [ ! -f "/app/data/openslides-logo.svg" ]; then
  cp /app/pkg/openslides-logo.svg /app/data/openslides-logo.svg
fi

if [ ! -f "/app/data/openslides-logo-dark.svg" ]; then
  cp /app/pkg/openslides-logo-dark.svg /app/data/openslides-logo-dark.svg
fi

echo "==> Migrating Database"
# Server migration
python3 /app/code/server/manage.py migrate

# Media service migration
PGPASSWORD="${CLOUDRON_POSTGRESQL_PASSWORD}" \
  psql -1 \
  -h "${CLOUDRON_POSTGRESQL_HOST}" \
  -p "${CLOUDRON_POSTGRESQL_PORT}" \
  -U "$CLOUDRON_POSTGRESQL_USERNAME" \
  -d "${CLOUDRON_POSTGRESQL_DATABASE}" \
  -f /app/code/media/src/schema.sql

echo "=> Ensure directories"
mkdir -p /run/nginx/{log,lib} /app/data/openslides

echo "=> Setting permissions"
chown -R cloudron:cloudron /run /app/data

echo "=> Starting OpenSlides"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i OpenSlides
