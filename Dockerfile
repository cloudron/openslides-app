FROM cloudron/base:2.0.0 AS base

ARG OPENSLIDES_VERSION=3.3

# node is only used to perform the UI build
ARG NODE_VERSION=13.14.0

RUN apt-get update && \
    apt-get -y install \
      # Base dependencies
      apt-transport-https \
      bzip2 \
      curl \
      g++ \
      gcc \
      git \
      gnupg2 \
      libpq-dev \
      make \
      postgresql-client \
      rsync \
      wait-for-it \
      wget \
      xz-utils \
      zlib1g-dev \
      libffi-dev \
      # Required utilities
      dnsutils \
      iputils-ping \
      netcat \
      procps \
      traceroute \
      # SAML Dependencies
      libxml2-dev \
      libxmlsec1-dev \
      libxmlsec1-openssl \
      pkg-config \
      # ASGI Server Dependencies
      python3-setuptools \
      gunicorn3 \
    && \
    rm -rf /var/cache/apt /var/lib/apt/lists

FROM base as build

RUN mkdir -p /app/{src,build} && \
    curl -L https://github.com/OpenSlides/OpenSlides/archive/${OPENSLIDES_VERSION}.tar.gz | tar zxf - --strip-components 1 -C /app/src && \
    chown -R cloudron:cloudron /app/src

FROM build as build-client

ENV PATH="/usr/local/node-${NODE_VERSION}/bin:$PATH"
ENV NPM_CONFIG_LOGLEVEL=error
ENV NPM_CONFIG_USER=cloudron

RUN npm install -g @angular/cli@^10
RUN ng config -g cli.warnings.versionMismatch false

WORKDIR /app/src/client
RUN npm install
RUN npm run build -- --output-path /app/build

FROM build as build-server

# Setup
RUN mkdir -p /app/build/server /app/build/media

# Main server
RUN pip3 install \
     -r /app/src/server/requirements/production.txt \
     -r /app/src/server/requirements/big_mode.txt \
     -r /app/src/server/requirements/saml.txt \
    # For the ASGI server / uvicorn
    uvloop \
    httptools
    
RUN cp /app/src/server/manage.py /app/build/server/manage.py && \
    cp -R /app/src/server/openslides /app/build/server/openslides

# Media service
RUN curl -L https://github.com/OpenSlides/openslides-media-service/archive/master.tar.gz | tar zxf - --strip-components 1 -C /app/build/media && \
    chown -R cloudron:cloudron /app/build/media && \
    pip3 install -r /app/build/media/requirements_production.txt && \
    # Remove unnecessary Files
    rm -f \
      /app/build/media/.gitignore \
      /app/build/media/Dockerfile \
      /app/build/media/*.sh \
      /app/build/media/*.txt

FROM base as final

# Final folder setup
RUN mkdir -p /app/pkg
COPY --from=build-client --chown=cloudron:cloudron /app/build /app/code/client
COPY --from=build-server --chown=cloudron:cloudron /app/build/server /app/code/server
COPY --from=build-server --chown=cloudron:cloudron /app/build/media /app/code/media
COPY --from=build-server --chown=cloudron:cloudron /usr/local/lib/python3.6/dist-packages /usr/local/lib/python3.6/dist-packages

# Add pkg folder and symlink relevant parts
ADD --chown=cloudron:cloudron pkg/ /app/pkg/
RUN ln -s /app/data/settings.py /app/code/server/settings.py &&\
    ln -s /app/pkg/cloudron_settings.py /app/code/server/cloudron_settings.py &&\
    ln -s /app/pkg/mediaservice-config.py /app/code/media/config.py &&\
    # Move logos into /app/pkg so they can be copied by start.sh conditionally.
    # This allows the stock logos to be optionally used or replaced directly in /app/data
    mv /app/code/client/assets/img/openslides-logo.svg /app/pkg && \
    ln -s /app/data/openslides-logo.svg /app/code/client/assets/img/openslides-logo.svg && \
    mv /app/code/client/assets/img/openslides-logo-dark.svg /app/pkg && \
    ln -s /app/data/openslides-logo-dark.svg /app/code/client/assets/img/openslides-logo-dark.svg

# Final supervisor and nginx setup
ADD supervisor/ /etc/supervisor/conf.d/
RUN rm -rf /var/log/nginx && ln -s /run/nginx/log /var/log/nginx && \
    rm -rf /var/lib/nginx && ln -s /run/nginx/lib /var/lib/nginx && \
    crudini --set /etc/supervisor/supervisord.conf supervisord logfile /run/supervisord.log && \
	  crudini --set /etc/supervisor/supervisord.conf supervisord logfile_backups 0

# Make web terminal start somewhere sensible
RUN mkdir -p /app/data && chown -R cloudron:cloudron /app/data
WORKDIR /app/data

CMD [ "/app/pkg/start.sh" ]
